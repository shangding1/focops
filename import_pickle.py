import pickle
import matplotlib.pyplot as plt

with open('focops_results/focops_velocity_Humanoid-v3_log_data_seed_0.pkl', 'rb') as f:
    data = pickle.load(f)

print(data.keys())

plt.plot(range(len(data['AvgR'])),data['AvgR'],color='r')
plt.ylabel("Avarage Reward", fontsize=15)
plt.xlabel("Iteration ", fontsize=15, labelpad=4)

plt.show()

plt.plot(range(len(data['AvgC'])),data['AvgC'],color='b')
plt.ylabel("Avarage Cost", fontsize=15)
plt.xlabel("Iteration ", fontsize=15, labelpad=4)

plt.show()